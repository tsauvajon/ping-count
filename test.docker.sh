#!/bin/bash

DELAY=${APP_DELAY:=5}

echo "waiting $DELAY seconds for the server to start"
sleep $DELAY
echo "testing $APP_URL"

# Count init à 0
if [ $(curl -s $APP_URL/count | ./jq-linux64 -r '.pingCount') -eq 0 ]
then
  echo "Serveur up ok"
else
  echo "Erreur : $APP_URL/count devrait renvoyer 0"
  exit 1
fi

# Ping => Pong
if [ $(curl -s $APP_URL/ping | ./jq-linux64 -r '.message')=="pong" ]
then
  echo "Pong ok"
else
  echo "Erreur: $APP_URL/ping devrait renvoyer 'pong'"
  exit 1
fi

# Count incrémenté
if [ $(curl -s $APP_URL/count | ./jq-linux64 -r '.pingCount') -eq 1 ]
then
  echo "Count ok"
else
  echo "Erreur: $APP_URL/count devrait renvoyer 1 après un appel sur /ping"
  exit 1
fi

echo "L'application est fonctionnelle"
exit 0
