# PingCount

## Dependencies

``` bash
# Install dep if you don't have it
go get -u github.com/golang/dep/cmd/dep
dep ensure
```

## Build

``` bash
go build
```

## Build the Docker image

``` bash
docker build -t ping-count .
```

## Run

``` bash
PORT=8088
SENTRY_DSN=${YOUR_SENTRY_TOKEN}
SENTRY_PROJECT=${YOUR_SENTRY_PROJECT_ID}
./ping-count
```

## Run in Docker

``` bash
PORT=80
SENTRY_DSN=${YOUR_SENTRY_TOKEN}
SENTRY_PROJECT=${YOUR_SENTRY_PROJECT_ID}
docker run --rm -p 8088:${PORT} -e SENTRY_DSN -e SENTRY_PROJECT -e PORT ping-count
```

## Consume the API

`127.0.0.1:${PORT}/ping`: answers with 'pong'

`/count`: answers with the number of ping requested

`/error`: creates and logs an error to Sentry

`/panic`: panics and logs it to Sentry

## Test

With Docker :

``` sh
# build the Docker image
docker build -t pingcounttest -f test.Dockerfile .

# run a container and open an interactive shell inside it
docker run -it pingcounttest sh

# run the test script
./test.docker.sh

# exit the container's shell
exit
```

On a Linux machine (requires `jq` to be installed) :

``` sh
./test.linux.sh
```
