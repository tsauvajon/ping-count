package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/getsentry/raven-go"
)

var pingCount = 0

// CountResponse is a response to /count
type CountResponse struct {
	Count int `json:"pingCount"`
}

// PingResponse is a response to /ping
type PingResponse struct {
	Msg string `json:"message"`
}

// Init initializes sentry and sets up the routes
func Init() {
	// Get the config from the environment
	dsn := os.Getenv("SENTRY_DSN")
	projectID := os.Getenv("SENTRY_PROJECT")

	// Build the url
	uri := fmt.Sprintf("https://%s@sentry.io/%s", dsn, projectID)

	// Init sentry
	raven.SetDSN(uri)

	fmt.Printf("Configured Sentry at %s\n", uri)

	// Routes
	http.HandleFunc("/ping", ping)
	http.HandleFunc("/count", count)
	http.HandleFunc("/error", logError)
	http.HandleFunc("/panic", logPanic)
}

func ping(w http.ResponseWriter, r *http.Request) {
	// Increments the counter after displaying the response
	defer func() {
		pingCount++
	}()

	// Create the response
	rsp := PingResponse{"pong"}

	// Write the JSON response
	json.NewEncoder(w).Encode(rsp)
}

func count(w http.ResponseWriter, r *http.Request) {
	// Create the response
	rsp := CountResponse{pingCount}

	// Write the response as JSON
	json.NewEncoder(w).Encode(rsp)
}

func logError(w http.ResponseWriter, r *http.Request) {
	// Captures an error
	raven.CaptureErrorAndWait(errors.New("Logging an error"), nil)
}

func logPanic(w http.ResponseWriter, r *http.Request) {
	// Captures a panic
	raven.CapturePanicAndWait(func() {
		panic("Oulala c'est la panique")
	}, nil)
}

func main() {
	// capture signals to a channel
	c := make(chan os.Signal, 2)

	// Both interruption signals are to be caught
	// It will prevent from quitting
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// Goroutine to asynchronously check the signals
	go func() {
		// Browse signals
		<-c

		fmt.Println("Exiting !")

		// Actually exit
		os.Exit(1)
	}()

	// Read the env variable $PORT
	port := os.Getenv("PORT")

	// Default value for $PORT
	if port == "" {
		port = "8088"
	}

	Init()

	// Notifying start
	fmt.Printf("Listening at :%v\n", port)

	// Start the http server
	http.ListenAndServe(":"+port, nil)
}
