#!/bin/sh

# Run l'application en background
nohup ./pingcount > /dev/null 2>&1 &

# Restaure la valeur précédente de $PORT
APP_URL=http://127.0.0.1:8088
echo "Started the app. Testing !"

# Count init à 0
if [ ! $(curl -s $APP_URL/count | jq -r '.pingCount') -eq 0 ]
then
  echo "Erreur : /count devrait renvoyer 0"
  exit 1
fi

# Ping => Pong
if [ ! $(curl -s $APP_URL/ping | jq -r '.message')=="pong" ]
then
  echo "Erreur: /ping devrait renvoyer 'pong'"
  exit 1
fi

# Count incrémenté
if [ ! $(curl -s $APP_URL/count | jq -r '.pingCount') -eq 1 ]
then
  echo "Erreur: /count devrait renvoyer 1 après un appel sur /ping"
  exit 1
fi

echo "L'application est fonctionnelle"
exit 0
