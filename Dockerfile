FROM golang:latest as builder

# Copy the local package files to the container's workspace.
WORKDIR /go/src/gitlab.com/epsi4/epsi-l4-c1/6/ping-count
COPY . .

ENV DEP_VERSION 0.4.1
# Get the dependencies
RUN curl -L -s https://github.com/golang/dep/releases/download/v${DEP_VERSION}/dep-linux-amd64 -o $GOPATH/bin/dep
RUN chmod +x $GOPATH/bin/dep
RUN dep ensure -vendor-only

# Build the app for alpine, executable name: ping-count
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ping-count

# Small container to run the app
FROM alpine:latest

WORKDIR /root/

# Copy the executable
COPY --from=builder /go/src/gitlab.com/epsi4/epsi-l4-c1/6/ping-count/ping-count .

# Run the app when the container starts
CMD [ "/root/ping-count" ]

EXPOSE 8088
